<?php

class BcaClass extends BasePaymentClass
{
    protected $merchantid;
    protected $apikey;
    protected $secretkey;
    protected $url;
    protected $paymentChannel = "VA_BCA";

    public function __construct($merchantid, $apikey, $secretkey, $url)
    {
        $this->merchantid = $merchantid;
        $this->apikey = $apikey;
        $this->secretkey = $secretkey;
        $this->url = $url;
    }

    // public function update($task, $transaction_id, $postData)
    // {

    // }

    public function payment($task, $param)
    {

        $bearerToken = $this->getToken();
        $TransactionDate = date('d/m/y H:i:s');
        $company_code = substr($param['payment_code'], 0, 5);
        $payment_code = substr($param['payment_code'], 5);
        //var_dump($paymentcode);


        $data = [

            "CompanyCode" => "$company_code",
            "CustomerNumber" => "$payment_code",
            "RequestID" => "201507131507262221400000001995",
            "ChannelType" => "6015",
            "CustomerName" => "Customer BCA Virtual Account",
            "CurrencyCode" => "IDR",
            "PaidAmount" => "$param[amount].00",
            "TotalAmount" => "$param[amount].00",
            "SubCompany" => "00001",
            "TransactionDate" => "$TransactionDate",
            "Reference" => "1234567890",
            "DetailBills" => null,
            "FlagAdvice" => "N",
            "Additionaldata" => "",

        ];

        $urlServer = $this->url . "/callback/va-bca/va/payments";

        $curl = curl_init();

        $arPost = array(
            //CURLOPT_PORT => "9000",
            CURLOPT_URL => $urlServer,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Authorization: Bearer " . $bearerToken,
                "Content-Type: application/json",
                "cache-control: no-cache",
            ),
        );

        curl_setopt_array($curl, $arPost);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $expected = json_decode('{
                "CompanyCode": "12345",
                "CustomerNumber": "1010014822119501",
                "RequestID": "201507131507262221400000001975",
                "PaymentFlagStatus": "00",
                "CustomerName": "Customer BCA Virtual Account",
                "CurrencyCode": "IDR",
                "TotalAmount": "11997.00",
                "Reference": "1234567890",
                "SubCompany": "00001",
                "PaidAmount": "11997.00",
                "TransactionDate": "14\/01\/2020 11:12:19",
                "ChannelType": "6014",
                "PaymentFlagReason": {
                    "English": "Success",
                    "Indonesian": "Sukses"
                }
            }', true);

            $status = "FAILED";
            if (strpos($response, 'Success') !== false) {
                $status = "PASS";
                $expected = json_decode($response, true);
            }

            return [
                'Task' => $task,
                'URL' => $this->url,
                'Request' => $data,
                'Response' => json_decode($response, true),
                'Expected' => $expected,
                'Status' => $status,
            ];

        }
    }

    public function getToken()
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            //CURLOPT_PORT => "9000",
            CURLOPT_URL => $this->url . "/callback/va-bca/api/oauth/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "grant_type=client_credentials",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Authorization: Basic YmNhdmFwYXltZW50OjE0OTg4MTU=",
                "Content-Type: application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $dataResult = json_decode($response);

            if (!empty($dataResult->access_token)) {
                return $dataResult->access_token;
            }

            // {
            //     "access_token": "ee66984c-4784-4731-9db0-8f3191a8ed55",
            //     "token_type": "bearer",
            //     "expires_in": 36004216,
            //     "scope": "resource.WRITE resource.READ"
            // }
        }
    }
}
