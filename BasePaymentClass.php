<?php

require_once 'vendor/autoload.php';

class BasePaymentClass extends AbstractPaymentClass
{

    protected $merchantid;
    protected $apikey;
    protected $secretkey;
    protected $url;

    public function registerMerchant()
    {
        echo "data";
    }

    public function activateMerchant()
    {
    }

    public function updateMerchant()
    {
    }

    public function registerChannelMerchant()
    {
    }

    public function listMerchant()
    {
    }

    public function listTransactionAdmin()
    {
    }

    /**
     * Merchant
     */
    public function create($task, $postData, $status_code = 200)
    {
        $url = $this->url . "/va/transactions?payment=" . $this->paymentChannel;
        $postData = $this->addSignature($postData);

        if ($task == 'Uppercase Signature Key') {
            $postData['signature'] = strtoupper($postData['signature']);
        }

        $result = $this->postData($url, $postData, 'POST');

        $expected = json_decode('
        {
            "status_message": "Success transaction",
            "status_code": 200,
            "process_id": "",
            "transaction_id": "50df1a93-af89-4135-a156-0b9aab671f28",
            "order_id": "12115543",
            "amount": 10696,
            "payment_code": "7888010007723993",
            "transaction_time": "2019-12-12T11:55:43.848+07:00",
            "expired_time": "2019-12-12T12:55:43.864+07:00"
        }', true);

        $status = 'FAILED';
        if ($status_code == 200) {
            $status = $result['response']['status_code'] == $status_code ? 'PASS' : 'FAILED';
            $expected = $status == 'FAILED' ? $expected : $result['response'];
        } else
        if ($status_code == 414) {
            $status = $result['response']['status_code'] == $status_code ? 'PASS' : 'FAILED';
            $expected = [
                "status_message" => "The request cannot be processed due to malformed syntax in the request body",
                "status_code" => 414,
                "timestamp" => "2019-12-12T12:34:56.556+07:00",
            ];
        } else
        // if ($status_code == 0) {
        //     $status = $result['response']['status_code'] == $status_code ? 'PASS' : 'FAILED';
        //     $expected = [
        //         "status_message" => "Expired time must be in future",
        //         "status_code" => '',
        //         "timestamp" => "2019-12-12T12:45:12.125+07:00",
        //     ];
        // } else
        if ($status_code == 400) {
            $status = $result['response']['status_code'] == $status_code ? 'PASS' : 'FAILED';

            $stringResponse = json_encode($result['response']);

            if (strpos($stringResponse, 'Amount cannot') !== false) {
                $expected = [
                    "status_message" => "Amount cannot below than 100",
                    "status_code" => 400,
                    "timestamp" => "2019-12-12T12:49:17.481+07:00",
                ];
            } else
            if ($task == 'Customer Info must filled') {
                $expected = [
                    "status_message" => "Customer Info must filled",
                    "status_code" => 400,
                    "timestamp" => "2019-12-12T12:49:17.481+07:00",
                ];
            } else
            if (strpos($stringResponse, 'Expired time must be in future') !== false) {
                $expected = [
                    "status_message" => "Expired time must be in future",
                    "status_code" => 400,
                    "timestamp" => "2019-12-12T12:49:17.481+07:00",
                ];
            } else
            if (strpos($stringResponse, 'Failed to request payment code. Error code 3') !== false) {
                $expected = [
                    "status_message" => "Failed to request payment code. Error code 3",
                    "status_code" => 400,
                    "timestamp" => "2019-12-12T12:49:17.481+07:00",
                ];
            } else
            if ($task == 'Amount must be filled by Integer') {
                $expected = [
                    "status_message" => "Amount must filled by Integer",
                    "status_code" => 400,
                    "timestamp" => "2019-12-12T12:49:17.481+07:00",
                ];
            } else
            if (strpos($task, 'Amount is required') !== false) {
                $expected = [
                    "status_message" => "Amount must be filled",
                    "status_code" => 400,
                    "timestamp" => "2019-12-12T12:49:17.481+07:00",
                ];
            } else
            if (strpos($task, 'Payment info is required') !== false) {
                $expected = [
                    "status_message" => "Payment info must be filled",
                    "status_code" => 400,
                    "timestamp" => "2019-12-12T12:49:17.481+07:00",
                ];
            } else
            if ($task == 'Payment info must be filled by Varchar') {
                $expected = [
                    "status_message" => "Payment info must be filled by Varchar",
                    "status_code" => 400,
                    "timestamp" => "2019-12-12T12:49:17.481+07:00",
                ];
            } else
            if (strpos($task, 'Customer info is required') !== false) {
                $expected = [
                    "status_message" => "Customer info must be filled",
                    "status_code" => 400,
                    "timestamp" => "2019-12-12T12:49:17.481+07:00",
                ];
            } else
            if ($task == 'Customer info must be filled by Varchar') {
                $expected = [
                    "status_message" => "Customer info must be filled by Varchar",
                    "status_code" => 400,
                    "timestamp" => "2019-12-12T12:49:17.481+07:00",
                ];
            } else
            if ($task == 'Order ID is required') {
                $expected = [
                    "status_message" => "Order ID can not be empty",
                    "status_code" => 400,
                    "timestamp" => "2019-12-12T12:49:17.481+07:00",
                ];
            } else
            if ($task == 'Order ID must be filled by Varchar') {
                $expected = [
                    "status_message" => "Order ID must be filled by Varchar",
                    "status_code" => 400,
                    "timestamp" => "2019-12-12T12:49:17.481+07:00",
                ];
            } else {
                $expected = [
                    "status_message" => "Failed to request payment code",
                    "status_code" => 400,
                    "timestamp" => "2019-12-12T12:49:17.481+07:00",
                ];
            }
        } else
        if ($status_code == 402) {
            $status = $result['response']['status_code'] == $status_code ? 'PASS' : 'FAILED';

            $expected = [
                "status_message" => "invalid Signature key",
                "status_code" => 402,
                "timestamp" => "2019-12-12T12:49:17.481+07:00",
            ];
        }

        return [
            'Task' => $task,
            'URL' => $result['urlServer'],
            'Request' => $result['request'],
            'Response' => $result['response'],
            'Expected' => $expected,
            'Status' => $status,
        ];
    }

    public function checkStatus($task, $tranid, $postData, $status_code = 200)
    {
        $url = $this->url . "/va/transactions/" . $tranid;

        $result = $this->postData($url, $postData, 'GET');

        $expected = json_decode('
        {
            "transaction_id": "$tranid",
            "amount": "14404",
            "payment_code": "5489001180002270",
            "customer_info": "Mas Mike",
            "payment_info": "payment 12120431",
            "status": "REQUEST",
            "order_id": "12120431",
            "merchant_id": "MCP20161201",
            "payment_channel": "VA_CIMB",
            "transaction_time": "2019-12-12T12:04:31.966+07:00",
            "expired_time": "2019-12-15T12:04:31.984+07:00",
            "transaction_fee_total": "5000"
        }', true);

        $status = 'FAILED';
        if (!empty($result['response']['payment_code'])) {
            $status = 'PASS';
            $expected = $result['response'];
        } else
        if ($status_code == 400) {
            $status = ($result['response']['status_code'] == $status_code) ? 'PASS' : 'FAILED';
            $expected = json_decode('
            {
                "status_message": "Payment Transaction not found",
                "status_code": 400,
                "timestamp": "2020-06-11T11:30:40.561+07:00"
            }', true);
        } else
        if ($status_code == 404) {
            $status = ($result['response']['status_code'] == $status_code) ? 'PASS' : 'FAILED';

            $expected = json_decode('
            {
                "transaction_id": "845739847",
                "amount": "$amount",
                "payment_code": "$paycod",
                "customer_info": "mike",
                "payment_info": "paym   ent 12120431",
                "status": "PAID",
                "order_id": "12120431",
                "merchant_id": "MCP20161201",
                "payment_channel": "VA_CIMB",
                "transaction_time": "2019-12-12T12:04:31.966+07:00",
                "expired_time": "2019-12-15T12:04:31.984+07:00",
                "transaction_fee_total": "5000"
            }', true);
        }

        return [
            'Task' => $task,
            'URL' => $result['urlServer'],
            'Request' => $result['request'],
            'Response' => $result['response'],
            'Expected' => $expected,
            'Status' => $status,
        ];

    }

    public function update($task, $tranid, $postData)
    {
        $url = $this->url . "/va/transactions/" . $tranid;

        $postData = $this->addSignature($postData);

        $result = $this->postData($url, $postData, 'PUT');

        $expected = json_decode('
        {
            "transaction_id": "573a638b-1b93-483e-b111-72e151be4cac",
            "amount": "14404",
            "payment_code": "5489001180002270",
            "customer_info": "Mas Mike",
            "payment_info": "payment 12120431",
            "status": "REQUEST",
            "order_id": "12120431",
            "merchant_id": "MCP20161201",
            "payment_channel": "VA_CIMB",
            "transaction_time": "2019-12-12T12:04:31.966+07:00",
            "expired_time": "2019-12-15T12:04:31.984+07:00",
            "transaction_fee_total": "5000"
        }', true);

        $status = 'FAILED';
        if (!empty($result['response']['payment_code'])) {
            $status = 'PASS';
            $expected = $result['response'];
        }

        return [
            'Task' => $task,
            'URL' => $result['urlServer'],
            'Request' => $result['request'],
            'Response' => $result['response'],
            'Expected' => $expected,
            'Status' => $status,
        ];
    }

    public function inquiry()
    {
    }

    public function delete($task, $tranid, $status_code = 200)
    {
        $url = $this->url . "/va/transactions/" . $tranid;

        $result = $this->postData($url, [], "DELETE");

        $expected = json_decode('

        {
            "process_id": "",
            "status_message": "Success cancel payment",
            "status_code": "200",
            "transaction_id": "573a638b-1b93-483e-b111-72e151be4cac",
            "order_id": "231",
            "amount": "900",
            "payment_code": "5489001180002270",
            "transaction_time": "2016-10-26T18:09:58.751+07:00",
            "expired_time": "2016-10-26T18:09:58.751+07:00"
        }', true);

        $status = 'FAILED';
        if (!empty($result['response']['payment_code'])) {
            $status = 'PASS';
            $expected = $result['response'];
        }

        return [
            'Task' => $task,
            'URL' => $result['urlServer'],
            'Request' => $result['request'],
            'Response' => $result['response'],
            'Expected' => $expected,
            'Status' => $status,
        ];
    }

    public function payment($task, $param)
    {
    }

    public function listTransaction()
    {
    }

    public function getDetailTransaction()
    {
    }

    public function postData($url, $postData = [], $method = 'GET')
    {
        // $url = 'https://hendro.free.beeceptor.com';

        $logTrans = [];
        $logTrans['urlServer'] = $url;
        $logTrans['apiKey'] = $this->apikey;
        $logTrans['request'] = $postData;

        try {
            //code...

            $client = new \GuzzleHttp\Client([
                'verify' => false,
                'headers' => [
                    'x-api-key' => $this->apikey,
                    'Content-type' => 'application/json',
                ],
            ]);

            // Send an asynchronous request.
            if ($method != 'GET' && count($postData) > 0) {
                $request = new \GuzzleHttp\Psr7\Request($method, $url, [], json_encode($postData));
            } else {
                $request = new \GuzzleHttp\Psr7\Request($method, $url);
            }

            $promise = $client->sendAsync($request);

            $response = $promise->wait();

            $logTrans['response'] = json_decode($response->getBody(true), true);

            return $logTrans;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            $logTrans['response'] =  json_decode($responseBodyAsString, true);

        }

        return $logTrans;
    }

    public function addSignature($data)
    {
        $paymentHash = $this->secretkey . $this->merchantid . $data['amount'] . $data['customer_info'] . $data['payment_info'] . $data['order_id'];
        $signatureForPaymentRequest = hash('sha256', $paymentHash);

        $data['mrc_id'] = $this->merchantid;
        $data['signature'] = $signatureForPaymentRequest;
        $data['stringhash'] = $paymentHash;

        return $data;
    }
}
