<?php

class CimbClass extends BasePaymentClass
{

    protected $merchantid;
    protected $apikey;
    protected $secretkey;
    protected $url;
    protected $paymentChannel = "VA_CIMB";

    public function __construct($merchantid, $apikey, $secretkey, $url)
    {
        $this->merchantid = $merchantid;
        $this->apikey = $apikey;
        $this->secretkey = $secretkey;
        $this->url = $url;
    }

    public function payment($task, $data)
    {

        $param = array(
            "ChannelID" => "PRM",
            "CompanyCode" => '5489',
            "CustomerKey1" => substr($data['payment_code'], 4, 80),
            "PaidAmount" => $data['amount'],
            "Amount" => $data['amount'],
            "CustomerName" => 'Mas Mike',
        );

        $TransactionID = date("Ymdhis");
        $TransactionDate = date("Ymdhis");

        $dataXML = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
        <SOAP-ENV:Header/>
            <SOAP-ENV:Body>
            <bil:CIMB3rdParty_PaymentRq xmlns:bil="http://CIMB3rdParty/BillPaymentWS">
                <bil:PaymentRq>
                <bil:TransactionID>' . $TransactionID . '</bil:TransactionID>
                <bil:ChannelID>' . $param['ChannelID'] . '</bil:ChannelID>
                <bil:TerminalID>PRR</bil:TerminalID>
                <bil:TransactionDate>' . $TransactionDate . '</bil:TransactionDate>
                <bil:CompanyCode>' . $param['CompanyCode'] . '</bil:CompanyCode>
                <bil:CustomerKey1>' . $param['CustomerKey1'] . '</bil:CustomerKey1>
                <bil:CustomerKey2/>
                <bil:CustomerKey3/>
                <bil:Language/>
                <bil:Currency>IDR</bil:Currency>
                <bil:Amount>' . $param['Amount'] . '</bil:Amount>
                <bil:Fee>0</bil:Fee>
                <bil:PaidAmount>' . $param['PaidAmount'] . '</bil:PaidAmount>
                <bil:ReferenceNumberTransaction>' . $TransactionDate . '</bil:ReferenceNumberTransaction>
                <bil:FlagPaymentList>1</bil:FlagPaymentList>
                <bil:CustomerName>' . $param['CustomerName'] . '</bil:CustomerName>
                <bil:AdditionalData1/>
                <bil:AdditionalData2/>
                <bil:AdditionalData3/>
                <bil:AdditionalData4/>
                </bil:PaymentRq>
            </bil:CIMB3rdParty_PaymentRq>
            </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>';
        // dd($dataXML);
        $curl = curl_init();

        $urlServer = $this->url . '/callback/va-cimb/vacimb.wsdl';

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "9000",
            CURLOPT_URL => $urlServer,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $dataXML,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: text/xml",
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {

            $expected = json_decode('
            {
                "TransactionID": "' . $TransactionID . '",
                "ChannelID": "PRM",
                "TerminalID": "PRR",
                "TransactionDate": "' . $TransactionID . '",
                "CompanyCode": "5489",
                "CustomerKey1": "' . substr($data['payment_code'], 4, 80) . '",
                "CustomerKey2": [],
                "CustomerKey3": [],
                "PaymentFlag": "1",
                "CustomerName": "Mas Mike",
                "Currency": "IDR",
                "Amount": "' . $data['amount'] . '",
                "Fee": "0",
                "PaidAmount": "' . $data['amount'] . '",
                "ReferenceNumberTransaction": "' . $TransactionID . '",
                "AdditionalData1": [],
                "AdditionalData2": [],
                "AdditionalData3": [],
                "AdditionalData4": [],
                "ResponseCode": "00",
                "ResponseDescription": "success"
            }', true);

            $status = "FAILED";
            $resultArray = $this->xmlToJson($response);
            if (!empty($resultArray)) {
                $status = "PASS";
                $response = json_encode($resultArray['SOAP-ENV_Body']['CIMB3rdParty_PaymentRs']['PaymentRs']);
            } 

            return [
                'Task' => $task,
                'URL' => $urlServer,
                'Request' => $data,
                'Response' => json_decode($response, true),
                'Expected' => $expected,
                'Status' => $status,
            ];

        }
    }

// FUNCTION TO MUNG THE XML SO WE DO NOT HAVE TO DEAL WITH NAMESPACE
    public function xmlToJson($xml)
    {
        $obj = SimpleXML_Load_String($xml);
        if ($obj === false) {
            return $xml;
        }

        // GET NAMESPACES, IF ANY
        $nss = $obj->getNamespaces(true);
        if (empty($nss)) {
            return $xml;
        }

        // CHANGE ns: INTO ns_
        $nsm = array_keys($nss);
        foreach ($nsm as $key) {
            // A REGULAR EXPRESSION TO MUNG THE XML
            $rgx
            = '#' // REGEX DELIMITER
             . '(' // GROUP PATTERN 1
             . '\<' // LOCATE A LEFT WICKET
             . '/?' // MAYBE FOLLOWED BY A SLASH
             . preg_quote($key) // THE NAMESPACE
             . ')' // END GROUP PATTERN
             . '(' // GROUP PATTERN 2
             . ':{1}' // A COLON (EXACTLY ONE)
             . ')' // END GROUP PATTERN
             . '#' // REGEX DELIMITER
            ;
            // INSERT THE UNDERSCORE INTO THE TAG NAME
            $rep
            = '$1' // BACKREFERENCE TO GROUP 1
             . '_' // LITERAL UNDERSCORE IN PLACE OF GROUP 2
            ;
            // PERFORM THE REPLACEMENT
            $xml = preg_replace($rgx, $rep, $xml);
        }

        if (strpos($xml, 'ns2_') !== false) {

            $replace = str_replace('ns2_', '', $xml);

            $resultArray = json_decode(json_encode(SimpleXML_Load_String($replace, 'SimpleXMLElement', LIBXML_NOCDATA)), true);

            return $resultArray;

        } else {
            return false;

        }
    }

}
