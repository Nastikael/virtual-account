<?php

class FinpayClass extends BasePaymentClass
{

    protected $merchantid;
    protected $apikey;
    protected $secretkey;
    protected $url;
    protected $paymentChannel = "VA_FINPAY";

    public function __construct($merchantid, $apikey, $secretkey, $url)
    {
        $this->merchantid = $merchantid;
        $this->apikey = $apikey;
        $this->secretkey = $secretkey;
        $this->url = $url;
    }

    public function payment($task, $data)
    {

    }

}
