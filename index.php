<?php

/**
 * 1. Add New Channel for nerchant MCP20161201
 * 2. Create New Class for channel (ex: VA_CIMB class name must be CimbClass)
 * 3. Add channel on line 42 ($payment_channel)
 * 4. Add Credential Merchant on line 58
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
error_reporting(0);

date_default_timezone_set("Asia/Jakarta");

/**
 * debug Function
 */
function debug($data, $exit = 0)
{
    echo "<pre>";
    print_r($data);
    echo "</pre><hr>";
    if ($exit) {
        exit();
    }
}

function dd($data)
{
    debug($data, 1);
}

/**
 * Main class
 */
include "AbstractPaymentClass.php";
include "BasePaymentClass.php";

/**
 * Active Payment Channel
 */
$payment_channel = [
    'VA_PERMATA',
    'VA_CIMB',
    'VA_FINPAY',
    'VA_BCA',
];

$url_server = [
    'Development' => 'http://10.40.30.190',
    'Sandbox' => 'https://api-sandbox.mcpayment.id',
    'Staging' => 'https://api-staging.mcpayment.id',
    'Prod Temp' => 'https://api-prod.mcpayment.id',
    'Production' => 'https://api.mcpayment.id',
];

$resultAll = [];
$url = $_REQUEST['url'];

$mrc_id = 'MCP20161201';
$apiKey = 'q6kpZa5YycdtfPakL2Cf';

/**
 * Credential Merchant
 */
if (isset($_REQUEST['channel']) && isset($_REQUEST['url'])) {
    $url = $_REQUEST['url'];
    $channel = $_REQUEST['channel'];

    if ($_REQUEST['channel'] == 'VA_PERMATA' && $_REQUEST['url'] == "https://api-prod.mcpayment.id") {
        $secret = 'zSNrDnp4bb';
    } else
    if ($_REQUEST['channel'] == 'VA_PERMATA' && $_REQUEST['url'] == "https://api-staging.mcpayment.id") {
        $secret = 'trsaOtlcfO';
    }

    if ($_REQUEST['channel'] == 'VA_CIMB') {
        $secret = 'rrIO7UfbaN';
    } else
    if ($_REQUEST['channel'] == 'VA_FINPAY') {
        $secret = 'rrIO7UfbaN';
    } else
    if ($_REQUEST['channel'] == 'VA_BCA') {
        $secret = 'WfO4HS2l03';
    }

    $exist_channel = false;
    foreach ($payment_channel as $value) {

        if ($channel == $value) {
            $exist_channel = true;

            $value = strtolower($value);
            $class = ucfirst(substr($value, 3));

            /**
             * Call Class
             */
            $class_name = $class . "Class";
            include $class_name . ".php";

            $vaPayment = new $class_name($mrc_id, $apiKey, $secret, $url);
        }
    }

    if (!$exist_channel) {
        dd("Channel Not Found");
    }

    $amount = rand(10000, 15000);


    /**
     * TEST CASE ONLY ONE
     */
    // $task = "Dynamic Payment Code - Wrong Expired time";

    // $postData = array(
    //     'amount' => $amount,
    //     'order_id' => date("dhis"),
    //     'customer_info' => 'Mas Mike',
    //     'payment_info' => 'payment ' . date("dhis"),
    //     "expired_time" => date('Y-m-d'),
    // );

    // $resultAll[] = $vaPayment->create($task, $postData, 414);
    // dd($resultAll);
    /**
     * END TEST CASE
     */


    /**
     * =========================================================
     * =========================================================
     */
     
     
     /**
      * 
      *
      *  BEGIN AUTO TEST
      *
      *
      *
      */
    //================================================= 1
    /* TC 1 */
    $task = "Dynamaic Create Virtual Account";

    if (isset($_REQUEST['amount'])) {
        $amount = $_REQUEST['amount'] * $amount;
    }

    $postData = array(
        'amount' => $amount,
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
    );

    $resultAll[0] = $vaPayment->create($task, $postData);
    /* END TC 1 */
    //================================================= 2
    $task = "Dynamic Payment Code - Wrong Expired time";

    $postData = array(
        'amount' => $amount,
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
        "expired_time" => date('Y-m-d'),
    );

    $resultAll[] = $vaPayment->create($task, $postData, 414);

    //================================================= 3
    $task = "Dynamic Payment Code - With 1 minutes expired";

    $postData = array(
        'amount' => $amount,
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
        "expired_time" => date('Y-m-d') . "T" . date('H:i:s', strtotime('+1 minutes', strtotime(date("Y-m-d H:i:s")))) . ".888+07:00",
    );

    $resulExpired = $vaPayment->create($task, $postData);

    if (!empty($resulExpired['Response']['payment_code'])) {
        $payment_code_expired = $resulExpired['Response']['payment_code'];
        $transactionId1_expired = $resulExpired['Response']['transaction_id'];
    }

    $resultAll[] = $resulExpired;

    //================================================= 4
    $task = "Dynamic Payment Code - Lower than now expiry date";

    $postData = array(
        'amount' => $amount,
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
        "expired_time" => date('Y-m-d') . "T" . date('H:i:s', strtotime('-6 hours', strtotime(date("Y-m-d H:i:s")))) . ".888+07:00",
    );

    $resultAll[] = $vaPayment->create($task, $postData, 400);

    //================================================= 5
    $task = "Dynamic Payment Code - minimal amount < 10000";

    $postData = array(
        'amount' => 100,
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
    );

    $resultAll[] = $vaPayment->create($task, $postData, 400);

    //================================================= 6
    $task = "invalid signature key";
    $postData = array(
        'amount' => "",
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
    );

    $resultAll[] = $vaPayment->create($task, $postData, 402);

    $paymentCode = $resultAll[0]['Response']['payment_code'];
    //================================================= 7
    $task = "Static Payment Code - Normal";
    $postData = array(
        'amount' => $amount,
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
        'payment_code' => strval($paymentCode + 1),
    );

    $staticPaymentCode = $vaPayment->create($task, $postData);

    $transactionId1 = '';
    if (!empty($staticPaymentCode['Response']['transaction_id'])) {
        $transactionId1 = $staticPaymentCode['Response']['transaction_id'];
    }

    $resultAll[] = $staticPaymentCode;

    //================================================= 8
    $task = "Static Payment Code - wrong Payment Code";
    $postData = array(
        'amount' => $amount,
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
        'payment_code' => "wrongpaymentcode",
    );

    $resultAll[] = $vaPayment->create($task, $postData, 400);

    //================================================= 9
    $task = "Static Payment Code - minimal amount < 10000";
    $postData = array(
        'amount' => 100,
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
        'payment_code' => strval($paymentCode + 2),
    );

    $resultAll[] = $vaPayment->create($task, $postData, 400);

    //================================================= 10
    $task = "Static Payment Code - With expiry time";
    $postData = array(
        'amount' => $amount,
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
        'payment_code' => strval($paymentCode + 2),
        "expired_time" => date('Y-m-d') . "T" . date('H:i:s', strtotime('+7 seconds', strtotime(date("Y-m-d H:i:s")))) . ".888+07:00",

    );

    $resultAll[] = $vaPayment->create($task, $postData);

    // ================================================= 11
    $task = "Static Payment Code - Too long static Payment Code";
    $postData = array(
        'amount' => $amount,
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
        'payment_code' => $paymentCode . "1231231231123123123123123",
    );

    $resultAll[] = $vaPayment->create($task, $postData, 400);

    //================================================= 12
    // $task = "Static Payment Code - from Status REQUEST";
    // $postData = array(
    //     'amount' => $amount,
    //     'order_id' => date("dhis"),
    //     'customer_info' => 'Mas Mike',
    //     'payment_info' => 'payment ' . date("dhis"),
    //     'payment_code' => strval($paymentCode),
    // );

    // $resultAll[] = $vaPayment->create($task, $postData, 00);

    //================================================= 13
    $task = "Amount is required";

    $postData = array(
        'amount' => '',
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
        'payment_code' => strval($paymentCode + 2),
    );

    $resultAll[] = $vaPayment->create($task, $postData, 400);

    //================================================= 14
    $task = "Amount must be filled by Integer";

    $postData = array(
        'amount' => 'seratus',
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
        'payment_code' => strval($paymentCode + 2),
    );

    $resultAll[] = $vaPayment->create($task, $postData, 400);
    //================================================= 15
    $task = "Payment info is required";

    $postData = array(
        'amount' => 10000,
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => '',
        'payment_code' => strval($paymentCode + 2),
    );

    $resultAll[] = $vaPayment->create($task, $postData, 400);
    //================================================= 16
    $task = "Payment info must be filled by Varchar";

    $postData = array(
        'amount' => 10000,
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => '%^^',
        'payment_code' => strval($paymentCode + 2),
    );

    $resultAll[] = $vaPayment->create($task, $postData, 400);

    //================================================= 17
    $task = "Customer info is required";

    $postData = array(
        'amount' => 10000,
        'order_id' => date("dhis"),
        'customer_info' => '',
        'payment_info' => 'payment ' . date("dhis"),
        'payment_code' => strval($paymentCode + 2),
    );

    $resultAll[] = $vaPayment->create($task, $postData, 400);
    //================================================= 18
    $task = "Customer info must be filled by Varchar";

    $postData = array(
        'amount' => 10000,
        'order_id' => date("dhis"),
        'customer_info' => '(&(*^%^%$)',
        'payment_info' => 'payment ' . date("dhis"),
        'payment_code' => strval($paymentCode + 2),
    );

    $resultAll[] = $vaPayment->create($task, $postData, 400);

    //================================================= 19
    $task = "Order ID is required";

    $postData = array(
        'amount' => 10000,
        'order_id' => '',
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
        'payment_code' => strval($paymentCode + 2),
    );

    $resultAll[] = $vaPayment->create($task, $postData, 400);
    //================================================= 20
    $task = "Order ID must be filled by Varchar";

    $postData = array(
        'amount' => 10000,
        'order_id' => '((*&*^',
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
        'payment_code' => strval($paymentCode + 2),
    );

    $resultAll[] = $vaPayment->create($task, $postData, 400);

    //================================================= 21
    $task = "Uppercase Signature Key";

    $postData = array(
        'amount' => 10000,
        'order_id' => '001',
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
        'payment_code' => strval($paymentCode + 2),
    );

    $resultAll[] = $vaPayment->create($task, $postData, 402);
    //================================================= 22
    $task = "Customer Info must filled";

    $postData = array(
        'amount' => '10000',
        'order_id' => '001',
        'customer_info' => '',
        'payment_info' => 'payment ' . date("dhis"),
        'payment_code' => strval($paymentCode + 2),
    );

    $resultAll[] = $vaPayment->create($task, $postData, 400);
    //================================================= 23
    // sleep(70);
    // flush();
    //================================================= 24
    $task = "Static Payment Code - from Status EXPIRED";
    $postData = array(
        'amount' => $amount,
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
        'payment_code' => strval($payment_code_expired),
    );

    $resultAll[] = $vaPayment->create($task, $postData);

//================================================= 25
    if (!empty($transactionId1)) {
        $task = "Update VA";
        $postData = array(
            'amount' => $amount,
            'order_id' => date("dhis"),
            'customer_info' => 'updated Mas Mike',
            'payment_info' => 'updated payment ' . date("dhis"),
        );

        $resultAll['test'] = $vaPayment->update($task, $transactionId1, $postData);
    }

//================================================= 26
    $task = "Payment VA";
    $postData = array(
        'amount' => $resultAll[0]['Response']['amount'],
        'payment_code' => strval($paymentCode),
    );
    $makePayment = $vaPayment->payment($task, $postData);
    $resultAll[] = $makePayment;
// ================================================= 26
    $transactionId = $resultAll[21]['Response']['transaction_id'];

    $task = "Check Status VA Before Payment";
    $resultAll[] = $vaPayment->checkStatus($task, $transactionId, [], 200);

    $task = "Check Status VA after payment";
    $transactionIdpayment = $resultAll[0]['Response']['transaction_id'];
    $resultAll[] = $vaPayment->checkStatus($task, $transactionIdpayment, [], 404);

    $task = "Check Status VA with invalid transaction";
    $resultAll[] = $vaPayment->checkStatus($task, '84762873648723643186482', [], 400);
// //================================================= 26

    $task = "Cancel VA when Transaction has been paid";
    $resultAll[] = $vaPayment->delete($task, $transactionId, 401);

// //================================================= 27
    if (!empty($transactionId1)) {
        $task = "Cancel VA";
        $resultAll['test'] = $vaPayment->delete($task, $transactionId1);
    }

    $task = "Check Status VA after canceled";
    $resultAll[] = $vaPayment->checkStatus($task, $transactionId1, [], 400);

    $postData = array(
        'amount' => $amount,
        'order_id' => date("dhis"),
        'customer_info' => 'Mas Mike',
        'payment_info' => 'payment ' . date("dhis"),
    );
    $createVa = $vaPayment->create($task, $postData);
    if (!empty($createVa['Response']['transaction_id'])) {
        $task = "Cancel VA with upper transaction id";
        $resultAll[] = $vaPayment->delete($task, strtoupper($createVa['Response']['transaction_id']), 404);
    }

    $task = "transaction id not found";
    $resultAll[] = $vaPayment->delete($task, '32489273482936423647523765426542', 404);
    $task = "transaction has been canceled";
    $resultAll[] = $vaPayment->delete($task, $transactionId1, 400);
    $task = "transaction has been canceled";
    $resultAll[] = $vaPayment->delete($task, $transactionId1, 400);
}

// 2. Callback VA***

// 2. List all transaction
// 2. Get detail transaction

/**
 * Admin
 */
// 2. Register Merchant
// 2. Activate Merchant
// 2. Update Merchant
// 2. List All Merchant
// 2. Register Payment Channel Merchant
// 2. List All Transaction

// 2. Static Payment Code - where PAID Status < 90 days ***
// 2. Static Payment Code - where PAID Status > 90 days ***

// dd($resultAll);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>VA API REPORT</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
    <style>
        table td {
            vertical-align: middle;
        }
    </style>
</head>

<body>


    <?php
echo $url . "<br/>";
echo "<br/>";
echo "<br/>";
echo "<br/>";
?>

    <form method="post">

        URL SERVER :
        <select name="url">
            <?php
foreach ($url_server as $name_env => $url) {
    $select_url = (isset($_REQUEST['url']) && $_REQUEST['url'] == $url) ? "selected" : "";
    echo '<option value="' . $url . '" ' . $select_url . '>' . $name_env . '</option>';
}
?>
        </select>

        <br />

        PAYMENT CHANNEL :
        <select name="channel">
            <?php
foreach ($payment_channel as $channel) {
    $select_channel = (isset($_REQUEST['channel']) && $_REQUEST['channel'] == $channel) ? "selected" : "";
    echo '<option value="' . $channel . '" ' . $select_channel . '>' . $channel . '</option>';
}
?>
        </select>

        <input type='submit' value='Button'>
    </form>

    <?php
if (isset($_REQUEST['channel']) && isset($_REQUEST['url'])) {
    echo "<br />";
    echo "<br />";
    echo "<hr>";

    echo "<table class='table table-bordered' style='font-size: 10px;'>";

    // header
    echo "
        <tr>
            <td><b>No</b></td>
            <td><b>Task</b></td>
            <td><b>URL Server</b></td>
            <td><b>Request</b></td>
            <td><b>Response</b></td>
            <td><b>Expected</b></td>
            <td><b>Status</b></td>
        </tr>
        ";

    // content
    foreach ($resultAll as $key => $value) {
        $zebra = ($key % 2 == 0) ? 'info' : '';

        echo "
            <tr class='" . $zebra . "'>
                <td>" . ($key + 1) . "</td>
                <td>" . $value['Task'] . "</td>
                <td>" . $value['URL'] . "</td>
                <td><pre>" . json_encode($value['Request'], JSON_PRETTY_PRINT) . "</pre></td>
                <td><pre>" . json_encode($value['Response'], JSON_PRETTY_PRINT) . "</pre></td>
                <td><pre>" . json_encode($value['Expected'], JSON_PRETTY_PRINT) . "</pre></td>
                <td>" . $value['Status'] . "</td>
            </tr>
        ";
    }
    echo "</table>";
}

?>

</body>

</html>

