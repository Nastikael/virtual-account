<?php

abstract class AbstractPaymentClass
{

    // protected $paymentChannel;

    /**
     * Admin
     */
    abstract public function registerMerchant();

    abstract public function activateMerchant();

    abstract public function updateMerchant();

    abstract public function registerChannelMerchant();

    abstract public function listMerchant();

    abstract public function listTransactionAdmin();

    /**`
     * Merchant
     */
    abstract public function create($task, $data, $status_code);

    abstract public function checkStatus($task, $tranid, $data);

    abstract public function update($task, $tranid, $data);

    abstract public function delete($task, $tranid, $status_code);

    abstract public function inquiry();

    abstract public function payment($task, $param);

    abstract public function listTransaction();

    abstract public function getDetailTransaction();
}
