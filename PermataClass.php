<?php

class PermataClass extends BasePaymentClass
{

    protected $merchantid;
    protected $apikey;
    protected $secretkey;
    protected $url;
    protected $paymentChannel = "VA_PERMATA";

    public function __construct($merchantid, $apikey, $secretkey, $url)
    {
        $this->merchantid = $merchantid;
        $this->apikey = $apikey;
        $this->secretkey = $secretkey;
        $this->url = $url;
    }

    public function payment($task, $param)
    {

        $TransactionID = date("Ymdhis");
        $TransactionDate = date('Y-m-d') . 'T' . date('H:i:s') . '.000+07:00';

        $data = [
            'PayBillRq' => [
                'INSTCODE' => '011',
                'VI_VANUMBER' => $param['payment_code'],
                'VI_TRACENO' => $TransactionID,
                'VI_TRNDATE' => $TransactionDate,
                'BILL_AMOUNT' => $param['amount'],
                'VI_CCY' => "360",
                'VI_DELCHANNEL' => "0096",

            ],
        ];

        $urlServer = $this->url . "/callback/va-permata/VirtualAccountH2H/service/PayBill";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "9000",
            CURLOPT_URL => $urlServer,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Authorization: Basic cDNybTR0NDo2ZTY1MmI2MzJkNzc4MzIwYmRhNWVhYzVjYTY4MDQwZTBkYjkwYzE1N2JiODZkNmExYzI1YTEzMGRlMzE2Nzlh",
                "Content-Type: application/json",
                "cache-control: no-cache",
                "x-api-key: " . $this->apikey,
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {

            $expected = [
                "payBillRs" => [
                    "STATUS" => "00",
                ],
            ];

            $status = "FAILED";
            if (strpos($response, '"STATUS":"00"') !== false) {
                $status = "PASS";
                $expected = json_decode($response, true);
            }

            return [
                'Task' => $task,
                'URL' => $urlServer,
                'Request' => $data,
                'Response' => json_decode($response, true),
                'Expected' => $expected,
                'Status' => $status,
            ];

        }
    }

}
